<?php

namespace App\Repository;

use App\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Comment>
 *
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    public function add(Comment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Comment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getByArticle($article, $currentPage = 1, $limit = 3)
    {
        if (empty($limit)) $limit = 1;
        $start = $currentPage * $limit - $limit;
        $query = $this->createQueryBuilder('c')
            ->andWhere('c.article = :article')
            ->setParameter('article', $article)
            ->orderBy('c.created', 'DESC')
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ;
        $paginator = new Paginator($query->getQuery(), false);

        return [
            'data' => $paginator->getQuery()->getArrayResult(),
            'current_page' => $currentPage,
            'max_page' => ceil($paginator->count() / $limit),
            'all_count' => $paginator->count(),
        ];
    }

    public function findAuthors($article, $author)
    {
        return $this->createQueryBuilder('c')
            ->select('c.authot')
            ->Where('c.article = :article')
            ->andWhere('c.authot LIKE :author')
            ->setParameter('article', $article)
            ->setParameter('author', $author.'%')
            ->orderBy('c.authot', 'ASC')
            ->groupBy('c.authot')
            ->getQuery()
            ->getArrayResult()
        ;
    }

    public function findCommentsByAuthor($article, $author)
    {
        return $this->createQueryBuilder('c')
            ->Where('c.article = :article')
            ->andWhere('c.authot = :author')
            ->setParameter('article', $article)
            ->setParameter('author', $author)
            ->orderBy('c.created', 'DESC')
            ->getQuery()
            ->getArrayResult()
            ;
    }

    public function findRand($article)
    {
        return $this->createQueryBuilder('c')
            ->Where('c.article = :article')
            ->setParameter('article', $article)
            ->orderBy('c.created', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getArrayResult()
            ;
    }




//    /**
//     * @return Comment[] Returns an array of Comment objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Comment
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
