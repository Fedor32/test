<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/comment")
 */
class CommentController extends AbstractController
{
    // число загружаемых комментариев в одном запросе
    protected int $comment_limit;

    public function __construct()
    {
        $this->comment_limit = 3;
    }

    /**
     * Возвращает массив комментариев для статьи
     * @Route("/getByArticle/", name="app_get_comment", methods={"POST", "GET"})
     */
    public function getByArticle(CommentRepository $repository, Request $request): Response
    {
        $params = $request->request->all();
        $result = [];
        if (!empty($params['article'])) {
            $currentPage = (int) $params['page'] ?? 1;
            $result = $repository->getByArticle($params['article'], $currentPage, $this->comment_limit);
        }
        return $this->json($result);
    }

    /**
     * Добавление коментария к статье
     * @Route("/create/", name="app_comment_create", methods={"POST"})
     */
    public function create(Request $request, ArticleRepository $articleRepository, CommentRepository $commentRepository): Response
    {
        $return = ['status'=>'error'];
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $article_id = $request->get('article');
            $article = $articleRepository->find((int)$article_id);
            if ($article) {
                $comment->setArticle($article);
                $commentRepository->add($comment, true);
                $comment_data = [
                    'id' => $comment->getId(),
                    'authot' => $comment->getAuthot(),
                    'text' => $comment->getText(),
                    'created' => $comment->getCreated()
                ];
                $return = ['status'=>'ok', 'comment'=>$comment_data];
            }
        }

        return $this->json($return);
    }

    /**
     * Возвращает массив авторов по началу имени
     * @Route("/findAuthors/", name="app_comment_find_authors", methods={"POST"})
     */
    public function findAuthors(Request $request, CommentRepository $repository): Response
    {
        $search = trim($request->get('author'));
        $article_id = $request->get('article');
        $authors = $repository->findAuthors($article_id, $search);
        return $this->render('comment/authors_list.html.twig', ['result'=>$authors]);
    }

    /**
     * Возвращает массив всех комментариев автора к данной статье
     * @Route("/findCommentsByAuthor/", name="app_comment_find_comments_authors", methods={"POST"})
     */
    public function findCommentsByAuthor(Request $request, CommentRepository $repository): Response
    {
        $search = $request->get('author');
        $article_id = $request->get('article');
        $comments = $repository->findCommentsByAuthor($article_id, $search);
        return $this->json($comments);
    }

}
