let Article = {
    id: 0,
    preloader: {
        element: '#comment-preloader',
        show: function () {$(Article.preloader.element).fadeIn(400);},
        hide: function () {$(Article.preloader.element).fadeOut(400);},
    },
    // ajax запросы
    ajax: async function(method, data={}, callback=null, showpreloader=false) {
        if (data === undefined) data = {}
        data.article = Article.id;
        if (showpreloader!==false) Article.preloader.show();
        method+= '/';
        $.post( method, data, function( result ) {
            if (callback!==null) callback(result);
            Article.preloader.hide();
        }).fail(function() {
            alert('Не удалось выполнит запрос');
            Article.preloader.hide();
        });
    },
    loadCommentsPage: function (page) {
        Article.ajax('/comment/getByArticle', {page:page}, loadCommentsPage_callback, true);
    }
}

let $result = $('#search_box-result');

$(function() {
    // Загрузить первые комментарии к статье
    Article.loadCommentsPage(1);

    // открыть модалку с формой добавления комментария
    $('#createCommentBut').on('click', NewCommentModalOpen);

    // подгрузить следующие комментарии
    $('#more_but').on('click', addComments);

    // поиск имен авторов соответствующих введенному в поле значению
    $('#search').on('keyup', findByAuthor);

    // валидация формы нового комментария перед отправкой
    $('#newCommentForm').parsley({successClass: 'is-valid', errorClass: 'is-invalid'})
        .on('form:submit', function(e) {
            let data = $('#newCommentForm').serialize();
            Article.ajax('/comment/create', data, newCommentForm_callback, true);
            return false;
        });
    // закрыть форму поиска автора при клике вне поля
    $(document).on('click', function(e){
        if (!$(e.target).closest('.search_box').length){
            $result.html('');
            $result.fadeOut(100);
        }
    });

    // найти комментарии автора к текущей статье
    $(document).on('click', '.search_result-name', function(){
        $('#search').val($(this).text());
        Article.ajax('/comment/findCommentsByAuthor/', {author:$(this).text()}, findCommentsByAuthor_callback, true)
        $result.fadeOut(100);
        return false;
    });

    // найти комментарии автора к текущей статье при жмяке по его имени в списке
    $(document).on('click', '.author-link', function() {
        Article.ajax('/comment/findCommentsByAuthor/', {author: $(this).text()}, findCommentsByAuthor_callback, true)
    })

    // активация слайдера
    $(document).ready(function() {
        $("#news-slider").owlCarousel({
            items:3,
            itemsDesktop:[1199,3],
            itemsDesktopSmall:[1000,2],
            itemsMobile:[650,1],
            pagination:false,
            navigationText:false,
            autoPlay:true
        });
    });
});

function findByAuthor() {
    let search = $(this).val();
    if (search.length > 2) {
        Article.ajax('/comment/findAuthors', {author:search}, findAuthors_callback, true);
        $result.fadeOut(100);
    } else {
        $result.html('');
        $result.fadeOut(100);
    }
    if (search === '') Article.loadCommentsPage(1);
}

function findCommentsByAuthor_callback(data) {
    $result.fadeOut(100);
    $('#comments-box').html('');
    $('#more_but').fadeOut();
    $('#comments_count').text(data.length + ' Отфильтровано по автору');
    $.each(data, function (i, comment) {
        $('#comments-box').append(createComment(comment));
    });
}

function findAuthors_callback(data) {
    $result.html(data);
    if(data !== ''){
        $result.fadeIn();
    } else {
        $result.fadeOut(100);
    }
}

function NewCommentModalOpen() {
    $('#newCommentForm').find("input[type=text], textarea").val("");
    $('#commentModal').modal('show');
}

function newCommentForm_callback(data) {
    if (data.status === 'ok') {
        $('#commentModal').modal('hide');
        $('#comments_count').text(1+Number($('#comments_count').text()));
        $('#comments-box').prepend(createComment(data.comment));
    }
}

function addComments() {
    $(this).find('i').fadeIn(200);
    let page = $(this).data('pageindex');
    Article.loadCommentsPage(page);
}

function loadCommentsPage_callback(data) {
    if (data.data.length>0) {
        $('#comments_count').text(data.all_count);
        $.each(data.data, function (i, comment) {
            $('#comments-box').append(createComment(comment));
        });
        if (data.current_page < data.max_page) {
            $('#more_but').data('pageindex', 1+data.current_page);
        } else {
            $('#more_but').fadeOut(400);
        }
        $('#more_but').find('i').fadeOut(400);
    }
}

// создать html для выводя комментария в список
function createComment(comment) {
    var options = { hour: 'numeric', minute: 'numeric', month: 'long', day: 'numeric' };
    var date_created  = new Date(comment.created.date);
    return '<div class="card"><div class="card-header"><i class="fa fa-clock"></i> '+date_created.toLocaleDateString("ru-RU", options)+' <strong><i class="fa fa-user"></i> <span class="author-link">'+comment.authot+'</span></strong></div><div class="card-body">'+comment.text+'</div></div>';
}

