### Установка
___
Клонировать репозиторий

Загрузить зависимости `composer install`

Настроить подключение к БД в файле `.env`

Выполнить миграции `php bin/console make:migration` `php bin/console doctrine:migrations:migrate`

Загрузить демоданные `php bin/console doctrine:fixtures:load`

Пользователь/пароль `admin@test.ru / 123123`

___
Очепятался при создании сущности Comment, обозвал поле authot, потом копировал не обращаая внимания... менять не стал


___
### Тестовое задание на должность “программист PHP”

Можно (но не обязательно) использовать любой фреймворк, как для бэка так и для фронта.

Суть задачи
Реализовать страницу с комментариями к произвольному статичному тексту.

Задачи:

* Реализовать добавление комментария, оно должно происходить без перезагрузки страницы. Новый комментарий должен быть отображён на странице сразу после добавления.

* Изначально показывать 3 комментария, остальные должны подгружаться (3 за один раз) по нажатию на кнопку “Показать ещё”.

* **Страница должна быть защищена авторизацией. Неавторизованному пользователю доступен только просмотр. Логин и пароль должны храниться в БД.

* **Реализовать фильтрацию комментариев по автору. Поле фильтра по автору должно быть текстовым. В процессе ввода (начиная с 3х символов) должны предлагаться варианты авторов. Автор = логин.

* **Реализовать слайдер с 5 случайными комментариями. Единовременно должны отображаться только 2 комментария остальные доступны при пролистывании. Слайдер зациклить (после последнего снова отображается первый) Слайдер расположить выше статичного текста.

Задачи со звёздочкой “*” не являются обязательными, но их выполнение приветствуется.